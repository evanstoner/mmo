// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Multi-Mission OpenShift',
  tagline: 'Guide to running multi-mission OpenShift environments.',
  url: 'https://evanstoner.gitlab.io',
  baseUrl: '/mmo/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'evanstoner', // Usually your GitHub org/user name.
  projectName: 'mmo', // Usually your repo name.

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl: 'https://gitlab.com/evanstoner/mmo/-/tree/main/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/evanstoner/mmo/-/tree/main/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
        gtag: {
          trackingID: 'G-MMRW2J3XMQ'
        }
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      announcementBar: {
        content: '<strong>🚧 This guide is a work in progress!</strong>',
        backgroundColor: '#ffcccf',
      },
      navbar: {
        title: 'MMO',
        logo: {
          alt: 'A fighter jet',
          src: 'img/fontawesome_jet.svg',
        },
        items: [
          {
            type: 'doc',
            docId: 'intro',
            position: 'left',
            label: 'The Guide',
          },
          {to: '/blog', label: 'Blog', position: 'left'},
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Docs',
            items: [
              {
                to: '/docs/intro',
                label: 'Introduction',
              },
              {
                to: '/docs/install/overview',
                label: 'Installation',
              },
              {
                to: '/docs/practices/about',
                label: 'Common Practices',
              },
            ],
          },
          {
            title: 'More',
            items: [
              {
                label: 'Blog',
                to: '/blog',
              },
              {
                label: 'GitLab',
                href: 'https://gitlab.com/evanstoner/mmo',
              },
              {
                label: 'Contributing',
                to: '/contributing',
              },
            ],
          },
          {
            title: 'Thanks',
            items: [
              {
                label: 'unDraw',
                href: 'https://undraw.co',
              },
              {
                label: 'Font Awesome',
                href: 'https://fontawesome.com',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} MMO maintainers. Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
