import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';

const FeatureList = [
  {
    title: 'Designed for Defense',
    Svg: require('../../static/img/undraw_maker_launch_re_rq81.svg').default,
    description: (
      <>
        This isn't the kind of quickstart where we turn off all the security controls&mdash;MMO
        is for teams supporting the most critical national security projects.
      </>
    ),
  },
  {
    title: 'Easy to Navigate',
    Svg: require('../../static/img/undraw_road_sign_re_3kc3.svg').default,
    description: (
      <>
        Rather than going deep on every concept, MMO provides quick explainers
        and examples, with links out to other resources if you need to learn more.
      </>
    ),
  },
  {
    title: 'Curated by Experts',
    Svg: require('../../static/img/undraw_teaching_re_g7e3.svg').default,
    description: (
      <>
        MMO was developed by the Red Hat solutions architects who work with the
        largest aerospace and defense organizations.
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} alt={title} />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
