# Contributing

## Style guide

- Guide page titles.
  - Use nouns vs. verbs. E.g. Use "Developer onboarding" not
    "Onboarding developers". This also means avoiding useless verbs, e.g. use
    "Quotas and limits" not "Configuring quotas and limits."
  - Use sentence capitalization. "Onboarding developers" not "Onboarding Developers".
- Guide page subtitles.
  - Use command verbs. E.g. use "Enforce quotas and limits"
    not "Enforcing quotas and limits."
- Guide content.
  - Use command verbs. E.g. use "Add more users by adding an identity provider" not
    "to add more users, add an identity provider."
