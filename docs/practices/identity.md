---
sidebar_position: 5
---

# Users and roles

## Add external users

_Official docs: [Understanding identity provider configuration](https://docs.openshift.com/container-platform/4.10/authentication/understanding-identity-provider.html)_

When you install an OpenShift cluster, only one user called `kubeadmin` will exist.
This is the equivalent of `root` on Linux. Add external users by configuring an **identity provider**. The simplest identity provider for testing is `htpasswd`,
but you will eventually use something like `ldap`, `oidc`, or others.

- [Review available identity providers](https://docs.openshift.com/container-platform/4.10/authentication/understanding-identity-provider.html#supported-identity-providers),
  then configure one.

# Apply roles

_Official docs: [Using RBAC to define and apply permissions](https://docs.openshift.com/container-platform/4.10/authentication/using-rbac.html)_

OpenShift uses roles to limit what users can do. In OpenShift, roles just represent
the API objects that a user can access (e.g. `Pod` or `Service`), and the HTTP verb
they can use with those objects (e.g. `GET` to list or `POST` to create). So there's
no ambiguity about what a given role actually permits. Additionally, roles can be
applied to individual users or groups, and scoped to the entire cluster or to a
single project.

- Review the [three types of users](https://docs.openshift.com/container-platform/4.10/authentication/understanding-authentication.html#rbac-users_understanding-authentication) and
  [three virtual groups](https://docs.openshift.com/container-platform/4.10/authentication/understanding-authentication.html#rbac-groups_understanding-authentication) so
  you understand what subjects are available to apply roles to.
- [Review the default cluster roles](https://docs.openshift.com/container-platform/4.10/authentication/using-rbac.html#default-roles_using-rbac). Only create new roles if
  these defaults do not satisfy your needs. Do not modify the default roles.
- Create and assign users to groups that match your organizational structure, or optionally
  [syncrhonize groups from LDAP](https://docs.openshift.com/container-platform/4.10/authentication/ldap-syncing.html).
- Apply roles to users or to groups. Importantly, assign `cluster-admin`
  to administrative users/groups. This enables your external administrative users
  to administer the cluster.

# Remove kubeadmin

_Official docs: [Removing the kubeadmin user](https://docs.openshift.com/container-platform/4.10/authentication/remove-kubeadmin.html)_

Once you have assigned the `cluster-admin` role to external users, remove the kubeadmin
user. This is akin to preventing `root` login on Linux.
