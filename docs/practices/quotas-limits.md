---
sidebar_position: 10
---

# Quotas and limits

## How usage is tracked in Kubernetes

_Kubernetes docs:_
- _[Resource Management for Pods and Containers](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/)_
- _[Resource Quotas](https://kubernetes.io/docs/concepts/policy/resource-quotas/)_
- _[Limit Ranges](https://kubernetes.io/docs/concepts/policy/limit-range/)_

Workloads in Kubernetes (i.e. pods and containers) specify **requests** and **limits**
for physical resources (e.g. CPU and memory). A request is the minimum amount of
the resource the workload needs, and the scheduler will ensure at least that much
is available. The limit is the maximum amount the workload is allowed to use, and
the scheduler may enforce the limit by terminating the workload.

**Quotas** are used to set constraints for aggregated requests, limits, and object
counts across all workloads in a project. In other words, a quota ensures that a
specific project cannot monopolize the cluster's resources. For example, if the
cluster has 64 GB of memory, we may allocate 8 GB to a specific project. You can
also use quotas to limit the number of Kubernetes objects (e.g. Pods or Secrets), 
but this is less common.

Within a project, **limit ranges** constrain requests and limits to ensure a specific
workload cannot monopolize the project's quota. For example, if the project is allocated
8 GB memory, we may not want a single pod to consume 2 GB of that. Without a limit
range, the pod can request all 8 GB.

## Configure a quota

_Official docs: [Sample resource quota definitions](https://docs.openshift.com/container-platform/4.11/applications/quotas/quotas-setting-per-project.html#quotas-sample-resource-quota-definitions_quotas-setting-per-project)_

Create a quota for each project where developers will have self-service capabilities.
This ensures the project will not monopolize the cluster's resources. Quotas can
be changed later if you discover the project needs more resources.

:::tip

Define "tee shirt size" quotas for quotas rather than having to discuss resource
specifics with every project team. For example, consider these quota definitions:

- Small: 2 CPU, 4 Gi memory
- Medium: 4 CPU, 8 Gi memory
- Large: 8 CPU, 16 Gi memory

:::

## Configure a limit range

_Offical docs: [Creating a Limit Range](https://docs.openshift.com/container-platform/4.11/nodes/clusters/nodes-cluster-limit-ranges.html#nodes-cluster-limit-creating_nodes-cluster-limit-ranges)_

The project administrator (not the cluster administrator) should configure limit
ranges for projects based on the needs of the applications deployed within the
project.