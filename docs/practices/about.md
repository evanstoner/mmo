---
sidebar_position: 1
---

# About common practices

This collection describes practices seen across many production OpenShift
environments. We refrain from the phrase "best practices" because every environment
has different requirements. Consider these practices as typical usage by your
peers, but consider what makes sense for you.
