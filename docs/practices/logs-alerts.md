---
sidebar_position: 15
---

# Log forwarding and alerting

## Forward logs to an external store

_Official docs: [Installing the logging subsystem for Red Hat OpenShift](https://docs.openshift.com/container-platform/4.10/logging/cluster-logging-deploying.html), [Forwarding logs to external third-party logging systems](https://docs.openshift.com/container-platform/4.10/logging/cluster-logging-external.html)_

## Forward alerts to an external notifier

_Official docs: [Configuring alert notifications](https://docs.openshift.com/container-platform/4.10/post_installation_configuration/configuring-alert-notifications.html)_
