---
sidebar_position: 75
---

# Upgrades

_Official docs: [Red Hat OpenShift Container Platform Life Cycle Policy](https://access.redhat.com/support/policy/updates/openshift)_

# Choose (and stick with) a regular update cadence

Whether you are internet-connected or air-gapped, choose an update cadence that
keeps you in support. OpenShift updates are released with a major, minor, and micro
version. For example, `4.11.13` is major version 4, minor version 11,
and micro version 13. 

We're on OpenShift major version 4 now, with no current plans in place for an
OpenShift 5.

Minor versions are released every 4 months with tons of new features, and are supported fully until 90 days
_after the next_ minor version is released (so that's about 7 months of full support 
for any minor release). Plan to upgrade your minor version
every 4 months, perhaps 30-60 days after the minor version is released. This
ensures the minor version has received any initial necessary fixes, provides plenty 
of time on that minor release, and gets you access to the latest features quickly.
Minor versions may include breaking changes, so review the release notes.

Micro versions are released continually with security and bugfixes and should be
applied more frequently, perhaps every 1-2 weeks. Because micro versions do not
introduce new features or remove old ones, micro versions are safe to apply
often.