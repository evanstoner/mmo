---
sidebar_position: 25
---

# Configuration management

## Use Git as the configuration source of truth

_Official docs: [Understanding OpenShift GitOps](https://docs.openshift.com/container-platform/4.11/cicd/gitops/understanding-openshift-gitops.html)_

Everything in Kubernetes is represented in YAML, including all OpenShift-specific
configuration described in this guide. Even if you completed an action in the web
GUI or command line, one or more objects were created behind the scenes. Store
these configurations in a Git repository to gain history/rollback, versioning,
and code review. Then, use **GitOps** to ensure the cluster configuration matches
the configuration in Git.

## Manage multiple clusters as a single fleet

Once you scale to multiple clusters (even as little as two, e.g. for "east" and "west"
or "dev" and "prod"), manage them as a single **fleet** instead of cluster-by-cluster.
Applying configurations at the fleet-level ensures that all clusters are configured
identically, reducing the chances that an application works on one cluster but not
on another. If you need to, you can partition your fleet with arbitrary labels.
For OpenShift clusters, use [Red Hat Advanced Cluster Management for Kubernetes](https://www.redhat.com/en/technologies/management/advanced-cluster-management) (or ACM for short). Configuring
fleet policy and configuration with ACM is beyond the scope of this guide, but as 
stated in the previous section, all configuration should be stored in Git, which
affords the same benefits to your fleet as it does to a single cluster.

Speak with your Red Hat account team to learn more about fleet management with ACM.

## Leverage existing compliance automation

_Official docs: [Understanding the Compliance Operator](https://docs.openshift.com/container-platform/4.11/security/compliance_operator/compliance-operator-understanding.html)_

For NIST 800-53 Moderate or High, NERC CIP, CIS Benchmark, and other common compliance
frameworks, install and leverage the Compliance Operator's built-in profiles.
The Compliance Operator scans and optionally remediates findings for both the 
_OpenShift platform layer_ and the _CoreOS operating system layer_. The Compliance
Operator also produces OpenSCAP XML, which is the same format auditors will be
familiar with from traditional compliance scans.