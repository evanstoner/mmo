---
sidebar_position: 40
---

# Air-gapped installation

To install OpenShift in an air-gapped environment, you'll need to download all of
the platform's container images, bring them into your environment, and populate
a container registry. Red Hat provides both the mirroring tool (`oc mirror`) and
the mirror registry. You may you use an existing container registry if you already
have one in the environment.

Here's a [video overview of OpenShift's disconnected tooling](https://youtu.be/1lhARQKdmNw?t=641)
from the OpenShift 4.10 product update.

## Create a mirror registry

_Official docs: [Creating a mirror registry with mirror registry for Red Hat OpenShift](https://docs.openshift.com/container-platform/4.10/installing/disconnected_install/installing-mirroring-creating-registry.html)_

Create a mirror registry only in the disconnected environment, and only if you do
not already have a registry you want to use. See the official docs for step-by-step
download and install instructions.

## Download platform images

:::info

This guide uses `oc mirror` for disconnected installations, which provides a much
friendlier experience than the previous `oc adm release mirror` process. However,
`oc mirror` is currently tech preview. (Only the command itself is tech preview;
a cluster deployed through this process _is_ supported.) If you will require support
of the install command, use the old method. See [Mirroring images for a disconnected installation](https://docs.openshift.com/container-platform/4.10/installing/disconnected_install/installing-mirroring-installation-images.html).

:::

_Official docs: [Mirroring images for a disconnected installation using the oc-mirror plug-in](https://docs.openshift.com/container-platform/4.10/installing/disconnected_install/installing-mirroring-disconnected.html)_

You'll use the `oc mirror` tool twice: once to download platform images _to disk_
from a connected network, and once on your disconnected network to populate a
container registry _from disk_. The first time you download platform images, it'll
be large since it's all of the images. In the subsequent runs, `oc mirror` only downloads
updated images, so updates are much smaller. This makes maintaining a disconnected
cluster easier.

Follow the instructions at the link above to download and run `oc mirror`.

## Deploy the cluster

_Official docs: [Installing a cluster on vSphere in a restricted network](https://docs.openshift.com/container-platform/4.10/installing/installing_vsphere/installing-restricted-networks-installer-provisioned-vsphere.html)_

Most air-gapped clusters are installed on vSphere, so follow the official docs
above.
