---
sidebar_position: 20
---

import Link from '@docusaurus/Link';
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Quick start (prod)

OpenShift makes it easy to stand up a production-worthy cluster using built-in
defaults. Of course, there are a lot of customizations you can make if necessary,
but start here first.

## 1. Configure DNS records

Your cluster will live at `<cluster_name>.<base_domain>`. The setup steps depend
on whether you are deploying to a public cloud (e.g. AWS or Azure) or on-prem
(e.g. vSphere).

<Tabs>
  <TabItem value="cloud" label="Public cloud">

Create a public DNS zone for `<base_domain>`.
The installer will handle the rest.

  </TabItem>
  <TabItem value="onprem" label="On-prem">

Create DNS records for `api.<cluster_name>.<base_domain>` and
`*.apps.<cluster_name>.<base_domain>`. Point them to any two unallocated IP's in
the subnet you're deploying to.

:::tip

For example: If your domain is `example.com` with the IP range 192.168.10.0/24,
your DHCP range is 192.168.10.50-250, and you want to name your cluster `demo`,
you could create entries for

- `api.demo.example.com` &rarr; 192.168.10.20
- `*.apps.demo.example.com` &rarr; 192.168.10.30

You don't have to use .20 and .30; you could use any IP's that are not in use, including by DHCP.

:::

  </TabItem>
</Tabs>

## 2. Download the `openshift-install` binary

Select the right binary based on your system. You can run the installer
from any system that can reach your cloud or virtualization provider.

<Tabs>
  <TabItem value="linux" label="Linux x86_64" default>
    <Link className="button button--primary"
      to="https://mirror.openshift.com/pub/openshift-v4/x86_64/clients/ocp/stable/openshift-install-linux.tar.gz">
      openshift-install (Linux x86_64)
    </Link>
  </TabItem>
  <TabItem value="mac" label="Mac x86_64" default>
    <Link className="button button--primary"
      to="https://mirror.openshift.com/pub/openshift-v4/x86_64/clients/ocp/stable/openshift-install-mac.tar.gz">
      openshift-install (Mac x86_64)
    </Link>
  </TabItem>
  <TabItem value="other" label="Other" default>
    Linux and Mac on x86_64 are curently the only supported platforms for running
    the installer.
  </TabItem>
</Tabs>

Extract with `tar zxf <file>` then make executable with `chmod +x openshift-install`

## 3. Copy your registry pull secret

The installer will need this secret to download container images from the Red Hat registry.

<Link className="button button--primary margin-bottom--md"
  to="https://console.redhat.com/openshift/install/platform-agnostic">
  Get your pull secret
</Link>

If you don't have a free Red Hat account yet, you can create one by following the link above.

## 4. Create a cluster

You'll be prompted for some configuration information, including the `cluster_name`
and `base_domain` from above.

```
./openshift-install create cluster
```

After 30-40 minutes, the login URL and credentials will be printed to the screen.
