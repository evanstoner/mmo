---
sidebar_position: 10
---

import Link from '@docusaurus/Link';

# Really quick start (non-prod)

If you just want to take OpenShift for a spin locally for non-production purposes,
OpenShift Local will set up a VM on your workstation for this. Follow the steps below:

<Link className="button button--primary margin-bottom--md"
  to="https://console.redhat.com/openshift/create/local">
  Get OpenShift Local
</Link>

If you don't have a free Red Hat account yet, you can create one by following the link above.
