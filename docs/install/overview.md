---
sidebar_position: 5
---

# Installation overview

_Official docs: [Installation overview](https://docs.openshift.com/container-platform/4.10/installing/index.html)_

So you're ready to start installing OpenShift! You've got a few options, let's
cover them now:

- **OpenShift can run in many environments.** You can run OpenShift on bare metal
  nodes, in hypervisors on-prem, and in public clouds (including government regions).
  If you have a public cloud account, this is usually the easiest way to get started
  with OpenShift. It's easy to set up, and tear down, a cluster.
- **OpenShift can run at different scales.** You can run a single node, three-node,
  or five+ node cluster (that's three control nodes, and two or more workers). For
  advanced cases, you can also dedicate _infrastructure_ nodes for certain workloads.
  A five node cluster is a typical cluster to start with. You can always scale it up.
- **OpenShift can run disconnected.** You can cache all of OpenShift's install
  dependencies and create clusters totally offline. For your first cluster, you
  should start connected to the internet for simplicity.
- **OpenShift can run in FIPS mode.** FIPS mode must be enabled at install-time;
  this requires setting a single flag in your `install-config.yaml`. We'll discuss
  that below.
- **OpenShift can be managed for you.** Red Hat and Microsoft or AWS co-support
  managed OpenShift offerings in their respective clouds, and are billed through
  your cloud account (including against committed spend programs).

## Fully automated installation

Throughout this guide, we refer to a command line tool `openshift-install` as
"the installer." The installer contains all the smarts and logic to orchestrate
the entire OpenShift installation process, including:

- Building virtual machines and other resources on a cloud or virtualization provider
  (or using out-of-band management to boot bare metal nodes)
- Bootstrapping the cluster and enrolling those newly-provisioned nodes
- Waiting for the cluster to stabilize

The installer by default will prompt for required installation parameters (e.g.
cloud credentials, DNS domains), but can also be configured with a file called
`install-config.yaml`. The `install-config.yaml` also allows you to make more
advanced configuration changes that are not available during the default prompt-based
deployment, such as enabling FIPS mode, changing virtual machine specs, and
adding proxy certificates.

This process is referred to as installer-provisioned infrastructure (IPI) and is
the recommended install process.

## "My environment is special and I can't automate all of that"

The fully automated installation process is opinionated about the environment and
therefore has some requirements around administrative access, DHCP, etc. For very
constrained environments, you may also provision infrastructure on your own before
the installer takes over. This is called user-provisioned infrastructure (UPI), and is
more work and error-prone. So, we recommend using installer-provisioned infrastructure
if at all possible.

We don't cover UPI in this guide because it can be complex, and deserves the full
details of the official OpenShift documentation. See
[Supported installation methods for different platforms](https://docs.openshift.com/container-platform/4.10/installing/installing-preparing.html#supported-installation-methods-for-different-platforms), and speak with your
Red Hat solutions architects.
