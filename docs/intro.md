---
sidebar_position: 1
---

# Introduction

Hi! Welcome! If you are planning to build an OpenShift environment to serve
internal customers, especially those with missions in the defense domain, you're
in the right place. The Multi-Mission OpenShift guide is here to help you understand
how to get started, and important practices used across the defense industry to
operate secure, multi-tenant clusters.

This guide is _not_ a comprehensive reference. Rather, it's a curated list of
common practices used across the industry, with examples to illustrate key points,
and links to deeper references when necessary. It's also not to be considered the
_only_ or _best_ way to provide a multi-mission OpenShift environment; you'll always
have to consider the unique needs and constraints of your programs.

## Why OpenShift? What about other kube distros?

Red Hat's contributions to the Kubernetes community, along with its long history
working with sensitive private and public sector customers, make OpenShift an
excellent starting point for organizations who need a low-effort, trustworthy
container platform. **OpenShift is CNCF-certified Kubernetes, with handy abstractions and common tooling built
in for operators _and_ developers (monitoring/alerting, logging, service mesh,
serverless, CI/CD, and more).** For you, that means less roll-your-own, fewer
integration headaches, and way more professional support. Plus, OpenShift runs
the same on metal, on-prem virtualization, and public cloud (including government
regions), which gives you standardization across domains.

So, could you apply this guide to other Kubernetes distributions? Sort of, but
you're signing up for a lot of integration work. We figured you'd rather focus on
more interesting stuff. 😉

## Where to learn more

This guide is just a start. To speak with the authors, and other solutions architects
who can provide deeper insights and experiences, reach out to your Red Hat account
team.
